package ar.edu.unju.fi.bean;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

import ar.edu.unju.fi.dao.UsuarioDao;
import ar.edu.unju.fi.dao.Impl.UsuarioDaoImpl;
import ar.edu.unju.fi.modelo.Usuario;
@ManagedBean(name="usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable{	
	private static final long serialVersionUID = 1L;
	private Usuario usuario;
//_____________________________________________________________________
	private String usuario2; 

	private int dni;
	private String nombre;
	private String apellido;
	private String clave;
	private String email;
 
	//______________________________________________________________________
    private int dni1; //para realizar la busqueda
    private String nombreUsuario1;//para realizar la busqueda
    private List<Usuario> usuarios;
  //________________________________________________________________________
    private String event;  
    private String event1;
    //--------------------------
    
    public Usuario getUsuario() {
 		return usuario;
 	}
 	public void setUsuario(Usuario usuario) {
 		this.usuario = usuario;
 	}
	public String getEvent1() {
		return event1;
	}
	public void setEvent1(String event1) {
		this.event1 = event1;
	}
	public int getDni1() {
		return dni1;
	}
	public String getUsuario2() {
		return usuario2;
	}
	public void setUsuario2(String usuario2) {
		this.usuario2 = usuario2;
	}
	

	public void setDni1(int dni1) {
		this.dni1 = dni1;
	}
	public String getNombreUsuario1() {
		return nombreUsuario1;
	}
	public void setNombreUsuario1(String nombreUsuario1) {
		this.nombreUsuario1 = nombreUsuario1;
	}
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
//___________________________________________________________________________	
	public String validarUsuario(){
	
		UsuarioDao usuarioDao = new UsuarioDaoImpl();
		Usuario usuario = usuarioDao.validarUsuario(usuario2, clave);	
		if (usuario != null){
			usuarios = usuarioDao.list();
		   	usuario2="";
			nombre= "";
		   	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
			return "home";
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			Application app = context.getApplication();
			ResourceBundle backendText = app.getResourceBundle(context, "msg");			
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, backendText.getString("error_ingreso"), backendText.getString("error_ingreso"));
	        FacesContext.getCurrentInstance().addMessage(null, message);			
			return null;
		}
	}

	//---------------------------------
//	public void limpiar() {
//		String nombreUsuario1 ="";
//		String dni1 ="";
//		String apellido="";
//		String nombre="";
//		String dni="";
//		String email="";
//		
//	}
//	//____________________________________________________________________________	
	public void nuevo(){
		usuario = new Usuario();
//		limpiar();
		event = "nuevo";
	}
//____________________________________________________________________________	
	public void modificar(){	
		event = "modificar";
	}
//___metodo buscar por nombre y dni a un usuario_____________________________	
	public void buscar(){
		UsuarioDao usuarioDao = new UsuarioDaoImpl();
		usuarios = usuarioDao.listByNombreByDni(nombreUsuario1, dni1);	
	}
//_____________________________________________________________________________
//	public String guardar(){
//    // usuario.setUsuario(usuario);
//		//usuario.setUsuario(usuario1); tomo usuario como la primary key
//		usuario.setUsuario(usuario);
//		usuario.setDni(dni);
//		usuario.setClave(clave);
//		usuario.setNombre(nombre);
//		usuario.setApellido(apellido);
//		usuario.setEmail(email);
//		
//		
//		UsuarioDao usuarioDao = new UsuarioDaoImpl();
//		
//		if (event.equals("modificar")){
//			usuarioDao.update(usuario);
//		}else{
//			usuarioDao.insert(usuario);
//			buscar();
//		}
//		RequestContext context = RequestContext.getCurrentInstance();
//        context.execute("PF('usuario').hide();");
//        context.update("form");
//        context.update("form:usuarios");
//		return "gestionUsuarios";
//	}
//_____________________________________________________________________________
		public String guardarMisDatos(){
			UsuarioDao usuarioDao = new UsuarioDaoImpl();
			if (event.equals("modificar")){
				usuarioDao.update(usuario);
			}else{
				usuarioDao.insert(usuario);
			}
		    buscar();
			RequestContext context = RequestContext.getCurrentInstance();
	        context.execute("PF('usuario').hide();");
	        context.update("form");
	        context.update("form:usuarios");
			return "misDatos";
		}	
//_____________________________________________________________________________	
	public String eliminar(){	
	System.out.println(usuario.getNombre());
		UsuarioDao usuarioDAO = new UsuarioDaoImpl();
		usuarioDAO.delete(usuario);
		buscar();
		RequestContext context = RequestContext.getCurrentInstance();
        context.update("form");
        context.update("form:usuarios"); 
		return "gestionUsuarios";
	}
//_____________________________________________________________________________	
	public String cerrar(){
        buscar();
		RequestContext context = RequestContext.getCurrentInstance();
        context.execute("PF('usuario').hide();");
        context.update("form");
        context.update("form:usuarios");
		return "gestionUsuarios.xhtml";
	}
//_____________________________________________________________________________	
		public String cerrarMisDatos(){
			buscar();
			RequestContext context = RequestContext.getCurrentInstance();
	        context.execute("PF('usuario').hide();");
	        context.update("form");
	        context.update("form:usuarios");  
			return "misDatos";
		}	
//_____________________________________________________________________________	
	public String salir(){
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuario");
		return "index";
		
	}
	
//_____________________________________________________________________________	
	public String misDatos(){
	    usuario =(Usuario)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		return "misDatos";
	}
//_____________________________________________________________________________	
	public String gestionUsuarios(){
		return "gestionUsuarios";
	}	
	
	public String cancelar() {
		
		
		return "home";
	}
}