package ar.edu.unju.fi.dao.Impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import ar.edu.unju.fi.dao.UsuarioDao;
import ar.edu.unju.fi.modelo.Usuario;
import ar.edu.unju.fi.hibernate.base.HibernateBase;;

public class UsuarioDaoImpl extends HibernateBase implements UsuarioDao{

	//Metodo para realizar el ingreso al sistema
	@Override
	public Usuario validarUsuario(String usuario2 , String clave) {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("usuario", usuario2));
		criteria.add(Restrictions.eq("clave", clave));
		return (Usuario) criteria.uniqueResult();
	}

	@Override
	public List<Usuario> list() {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		return criteria.list();		 
	}
	// Metodo para realizar la busqueda de un usuario por dni y nombre
	@Override
	public List<Usuario> listByNombreByDni(String nombreUsuario1, int dni1) {
		Criteria criteria = getSession().createCriteria(Usuario.class);
		if (dni1 != 0){
			criteria.add(Restrictions.eq("dni", dni1));	
		}
		if(!nombreUsuario1.isEmpty()){
			criteria.add(Restrictions.ilike("nombre", nombreUsuario1, MatchMode.ANYWHERE));			
		}		
		return criteria.list();
	}
	
	//metodo para insertar un nuevo usuario
	@Override
	public void insert(Usuario usuario) {
       try{
          Transaction transaction = getSession().beginTransaction();
          getSession().save(usuario);
          transaction.commit();
       }catch(HibernateException e){
           e.printStackTrace();
      }
}
	//Metodo para actualizar un usuario
	@Override
	public void update(Usuario usuario) {
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().update(usuario);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	      }		
	}
	//metodo para eliminar un usuario
	@Override
	public void delete(Usuario usuario) {
		try{
	          Transaction transaction = getSession().beginTransaction();
	          getSession().delete(usuario);
	          transaction.commit();
	       }catch(HibernateException e){
	           e.printStackTrace();
	      }			
	}
}
